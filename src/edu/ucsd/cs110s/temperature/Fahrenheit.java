/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author amquinte
 *
 */
public class Fahrenheit extends Temperature
{
	
	public Fahrenheit(float t)
	{
		super(t);
	}
	
	public String toString()
	{
		// TODO: Complete this method
		String tmp = "" + this.getValue();
		return tmp;
	}

	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		Celsius tmp = new Celsius((float)(((this.getValue()-32)*5))/9);
		return tmp;
	}

	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return this;
	}
}
