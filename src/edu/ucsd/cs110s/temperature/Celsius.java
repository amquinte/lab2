/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author amquinte
 *
 */
public class Celsius extends Temperature
{
	
	public Celsius(float t)
	{
		super(t);
	}
	
	public String toString()
	{
		// TODO: Complete this method
		String tmp = "" + this.getValue();
		return tmp;
	}

	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		Fahrenheit tmp = new Fahrenheit((float)((this.getValue()*9)/5 + 32));
		return tmp;
	}
}
